import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { ServerScreen, ServersScreen } from "../../screens";
import { colors } from "../../styles";
import { ServersStackParamList } from "./servers-navigator-types";

const Stack = createStackNavigator<ServersStackParamList>();

const ServersNavigator: React.FC = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: colors.violetGray2,
          height: 60,
        },
        headerTintColor: colors.white2,
        headerTitleAlign: "center",
      }}
    >
      <Stack.Screen
        name="Servers"
        component={ServersScreen}
        options={{ title: "Recruitment Task" }}
      />
      <Stack.Screen
        name="Server"
        component={ServerScreen}
        options={({ route }) => ({ title: route.params.server.name })}
      />
    </Stack.Navigator>
  );
};

export default ServersNavigator;
