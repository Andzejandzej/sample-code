import { ServerType } from "../../services";

export type ServersStackParamList = {
  Server: { server: ServerType };
  Servers: undefined;
};
