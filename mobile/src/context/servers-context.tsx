import React, { createContext, useState, useEffect } from "react";
import { ServerStatuses } from "../constants";
import { ServerType, ServersType, api } from "../services";
import { sortAlphabeticallyByKey } from "../utils";

type setServerStatusType = (
  serverId: number,
  targetStatus: "on" | "off"
) => Promise<void>;

type rebootServerType = (serverId: number) => Promise<void>;

type serversContextType = {
  rebootServer: rebootServerType;
  servers: ServersType;
  setServerStatus: setServerStatusType;
};

export const ServersContext = createContext({} as serversContextType);

export const ServersContextProvider: React.FC = ({ children }) => {
  const [servers, setServers] = useState<ServersType>([]);

  useEffect(() => {
    (async () => {
      try {
        const { data: rawServers } = await api.getServers();
        const sortedServers = sortAlphabeticallyByKey(rawServers, "name");
        setServers(sortedServers);
      } catch (error) {
        console.error(error);
      }
    })();
  }, []);

  const setServerStatus: setServerStatusType = async (
    serverId,
    targetStatus
  ) => {
    try {
      const { data: updatedServer } = await api.setServerStatus(
        serverId,
        targetStatus
      );
      updateServerStatus(updatedServer);
    } catch (error) {
      console.error(error);
    }
  };

  const rebootServer: rebootServerType = async (serverId) => {
    try {
      const { data: updatedServer } = await api.rebootServer(serverId);
      updateServerStatus(updatedServer);
      pingRebootingServer(serverId);
    } catch (error) {
      console.error(error);
    }
  };

  type pingRebootingServerType = (serverId: number) => Promise<void>;

  const pingRebootingServer: pingRebootingServerType = async (serverId) => {
    try {
      const { data: updatedServer } = await api.getServer(serverId);
      if (updatedServer.status === ServerStatuses.REBOOTING) {
        setTimeout(() => pingRebootingServer(serverId), 1000);
      } else {
        updateServerStatus(updatedServer);
      }
    } catch (error) {
      console.error(error);
    }
  };

  type updateServerStatusType = (updatedServer: ServerType) => void;

  const updateServerStatus: updateServerStatusType = (updatedServer) => {
    setServers((previousList) => {
      const updatedServerList = previousList.map((server) => {
        if (server.id === updatedServer.id) {
          server.status = updatedServer.status;
        }
        return server;
      });
      return updatedServerList;
    });
  };

  return (
    <ServersContext.Provider
      value={{
        rebootServer,
        servers,
        setServerStatus,
      }}
    >
      {children}
    </ServersContext.Provider>
  );
};
