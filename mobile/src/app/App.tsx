import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import { ServersContextProvider } from "../context";
import { ServersNavigator } from "../navigation";

const App: React.FC = () => {
  return (
    <NavigationContainer>
      <ServersContextProvider>
        <ServersNavigator />
      </ServersContextProvider>
    </NavigationContainer>
  );
};

export default App;
