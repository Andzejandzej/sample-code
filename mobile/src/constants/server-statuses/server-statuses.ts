export enum ServerStatuses {
  OFFLINE = "OFFLINE",
  ONLINE = "ONLINE",
  REBOOTING = "REBOOTING",
}
