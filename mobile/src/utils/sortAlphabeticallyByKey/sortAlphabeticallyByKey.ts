const sortAlphabeticallyByKey = <T, K extends keyof T>(
  array: Array<T>,
  key: K
): Array<T> => {
  return [...array].sort((a, b) => {
    if (a[key] > b[key]) {
      return 1;
    } else if (a[key] < b[key]) {
      return -1;
    }
    return 0;
  });
};

export default sortAlphabeticallyByKey;
