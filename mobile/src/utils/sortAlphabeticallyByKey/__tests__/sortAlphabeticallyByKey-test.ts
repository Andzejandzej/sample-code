import sortAlphabeticallyByKey from "..";

const rawArray = [
  {
    someKey: "b",
  },
  {
    someKey: "c",
  },
  {
    someKey: "a",
  },
];

const sortedArray = [
  {
    someKey: "a",
  },
  {
    someKey: "b",
  },
  {
    someKey: "c",
  },
];

it("sorts correctly by provided key", () => {
  expect(sortAlphabeticallyByKey(rawArray, "someKey")).toEqual(sortedArray);
});
