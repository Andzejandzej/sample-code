import React from "react";
import { View, StyleSheet } from "react-native";
import { colors } from "../../styles";

const ServerRowSeparator: React.FC = () => {
  return <View style={styles.separator} />;
};

const styles = StyleSheet.create({
  separator: {
    backgroundColor: colors.lightGray,
    borderLeftColor: colors.white,
    borderLeftWidth: 28,
    height: 1,
  },
});

export default ServerRowSeparator;
