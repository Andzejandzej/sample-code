import React from "react";
import { View, Text, TouchableNativeFeedback, StyleSheet } from "react-native";
import { colors } from "../../styles";

type AcceptServerStatusChangeBtnProps = {
  onPressHandler: () => void;
};

const AcceptServerStatusChangeBtn: React.FC<AcceptServerStatusChangeBtnProps> = ({
  onPressHandler,
}) => (
  <TouchableNativeFeedback accessibilityRole="button" onPress={onPressHandler}>
    <View style={styles.button}>
      <Text style={styles.buttonText}>Accept</Text>
    </View>
  </TouchableNativeFeedback>
);

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    backgroundColor: colors.violet,
    borderRadius: 5,
    height: 65,
    justifyContent: "center",
    marginTop: 20,
    width: "90%",
  },
  buttonText: {
    color: colors.white,
    fontSize: 20,
    fontWeight: "bold",
  },
});

export default React.memo(AcceptServerStatusChangeBtn);
