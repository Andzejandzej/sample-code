import React from "react";
import { FlatList, StyleSheet, View } from "react-native";
import {
  ServersTableHeader,
  ServerRowSeparator,
  ServerRow,
} from "../../components";
import { ServersType } from "../../services";
import { colors } from "../../styles";

type ServersTableProps = {
  servers: ServersType | [];
};

const ServersTable: React.FC<ServersTableProps> = ({ servers }) => {
  return (
    <View style={styles.list}>
      <FlatList
        keyExtractor={(server) => server.id.toString()}
        data={servers}
        renderItem={({ item }) => <ServerRow server={item} />}
        ListHeaderComponent={ServersTableHeader}
        stickyHeaderIndices={[0]}
        ItemSeparatorComponent={ServerRowSeparator}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  list: {
    backgroundColor: colors.white,
    borderTopLeftRadius: 5,
    flex: 1,
    marginLeft: 28,
    marginTop: 28,
  },
});

export default ServersTable;
