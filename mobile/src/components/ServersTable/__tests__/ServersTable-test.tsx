import React from "react";
import { render } from "react-native-testing-library";
import ServersTable from "../";
import { ServersType } from "../../../services";

jest.mock("@react-navigation/native");

const mockedServers: ServersType = [
  {
    id: 1,
    name: " US East (Virginia)",
    status: "ONLINE",
  },
  {
    id: 2,
    name: " US East (Ohio)",
    status: "ONLINE",
  },
  {
    id: 3,
    name: "US West (N. California)",
    status: "OFFLINE",
  },
  {
    id: 4,
    name: " US West (Oregon)",
    status: "ONLINE",
  },
];

describe("<ServersTable />", () => {
  it("Renders correct number of ServerRow's", () => {
    const { getAllByTestId } = render(<ServersTable servers={mockedServers} />);
    expect(getAllByTestId("server-row")).toHaveLength(4);
  });
});
