import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/Entypo";
import { ServerStatuses } from "../../constants";
import { ServersStackParamList } from "../../navigation";
import { ServerType } from "../../services";
import { colors } from "../../styles";
import ServerStatusText from "../ServerStatusText";

type ServerRowProps = {
  server: ServerType;
};

const ServerRow: React.FC<ServerRowProps> = ({ server }) => {
  const navigation = useNavigation<
    StackNavigationProp<ServersStackParamList, "Servers">
  >();
  return (
    <View style={styles.row} testID="server-row">
      <Text style={styles.name}>{server.name}</Text>
      <View style={styles.status}>
        <ServerStatusText status={server.status} />
      </View>
      <View style={styles.actions}>
        {server.status !== ServerStatuses.REBOOTING && (
          <Icon.Button
            name="dots-three-vertical"
            size={28}
            iconStyle={styles.btnIcon}
            backgroundColor={colors.white}
            accessible={true}
            accessibilityLabel={"Go to server screen"}
            onPress={() => {
              navigation.navigate("Server", {
                server: server,
              });
            }}
          />
        )}
      </View>
    </View>
  );
};

export default ServerRow;

const styles = StyleSheet.create({
  actions: {
    alignItems: "center",
    flex: 60,
    justifyContent: "center",
  },
  btnIcon: {
    color: colors.lightGray2,
    marginRight: 0,
  },
  name: {
    color: colors.violetGray,
    flex: 215,
  },
  row: {
    alignItems: "center",
    flexDirection: "row",
    height: 62,
    justifyContent: "space-between",
    paddingLeft: 28,
  },
  status: {
    flex: 95,
  },
});
