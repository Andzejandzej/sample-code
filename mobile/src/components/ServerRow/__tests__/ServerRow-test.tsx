import React from "react";
import { render, fireEvent } from "react-native-testing-library";
import { ServerStatuses } from "../../../constants";
import { ServerType } from "../../../services";
import ServerRow from "../ServerRow";

const mockOnPressHandler = jest.fn();

jest.mock("@react-navigation/native", () => ({
  useNavigation: () => ({
    navigate: mockOnPressHandler,
  }),
}));

const mockedOnlineServer: ServerType = {
  id: 4,
  name: "some name",
  status: ServerStatuses.ONLINE,
};

const mockedOfflineServer: ServerType = {
  id: 4,
  name: "some name",
  status: ServerStatuses.OFFLINE,
};

const mockedRebootingServer: ServerType = {
  id: 4,
  name: "some name",
  status: ServerStatuses.REBOOTING,
};

describe("<ServerRow />", () => {
  it("Shows three dots button when server status is 'ONLINE'", () => {
    const { getAllByA11yLabel } = render(
      <ServerRow server={mockedOnlineServer} />
    );
    expect(getAllByA11yLabel("Go to server screen")).toHaveLength(1);
  });

  it("Shows three dots button when server status is 'OFFLINE'", () => {
    const { getAllByA11yLabel } = render(
      <ServerRow server={mockedOfflineServer} />
    );
    expect(getAllByA11yLabel("Go to server screen")).toHaveLength(1);
  });

  it("Hides three dots button when server status is 'REBOOTING'", () => {
    const { getAllByA11yLabel } = render(
      <ServerRow server={mockedRebootingServer} />
    );
    expect(() => getAllByA11yLabel("Go to server screen")).toThrow();
  });

  it("Calls navigate with adequate params when three dots button is pressed", () => {
    const { getByA11yLabel } = render(
      <ServerRow server={mockedOnlineServer} />
    );
    const instance = getByA11yLabel("Go to server screen");
    fireEvent.press(instance);
    expect(mockOnPressHandler).toHaveBeenCalledWith("Server", {
      server: mockedOnlineServer,
    });
  });
});
