import CheckBox from "@react-native-community/checkbox";
import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { colors } from "../../styles";

type ServerStatusCheckboxProps = {
  isChecked: boolean;
  label: string;
  onCheckHandler: () => void;
};

const ServerStatusCheckbox: React.FC<ServerStatusCheckboxProps> = ({
  label,
  isChecked,
  onCheckHandler,
}) => (
  <View style={styles.container}>
    <CheckBox
      value={isChecked}
      onChange={onCheckHandler}
      disabled={isChecked}
      tintColors={{ false: colors.violetGray2, true: colors.violetGray2 }}
    />
    <Text style={styles.text}>{label}</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flexDirection: "row",
    height: 80,
    marginLeft: 50,
    width: "100%",
  },
  text: {
    color: colors.violetGray2,
    fontSize: 20,
    marginLeft: 15,
  },
});

export default React.memo(ServerStatusCheckbox);
