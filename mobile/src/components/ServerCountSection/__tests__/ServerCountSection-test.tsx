import React from "react";
import { render } from "react-native-testing-library";
import ServerCountSection from "../ServerCountSection";

describe("<ServerCountSection />", () => {
  it("Renders correct number of servers", () => {
    const { getByText } = render(<ServerCountSection serverCount={4} />);
    expect(getByText("Number of elements: 4")).toBeDefined();
  });
});
