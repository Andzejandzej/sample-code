import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { colors } from "../../styles";

const ServerCountSection: React.FC<{ serverCount: number }> = ({
  serverCount,
}) => (
  <View style={styles.container}>
    <Text style={styles.sectionTitle}>Servers</Text>
    <Text style={styles.serverCountText}>
      Number of elements: {serverCount}
    </Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    marginLeft: 28,
  },
  sectionTitle: {
    color: colors.violetGray2,
    fontSize: 24,
    fontWeight: "bold",
  },
  serverCountText: {
    color: colors.violetGray3,
    fontSize: 17,
  },
});

export default React.memo(ServerCountSection);
