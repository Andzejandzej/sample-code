import React from "react";
import { View, TextInput, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import { colors } from "../../styles";

type SearchBarProps = {
  setSearchPhrase: (phrase: string) => void;
};

const SearchBar: React.FC<SearchBarProps> = ({ setSearchPhrase }) => {
  return (
    <View style={styles.container}>
      <Icon
        style={styles.icon}
        name="search1"
        color={colors.lightGray3}
        size={18}
      />
      <TextInput
        style={styles.input}
        placeholder="Search..."
        placeholderTextColor={colors.lightGray3}
        onChangeText={(phrase) => setSearchPhrase(phrase)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: colors.white,
    borderRadius: 5,
    flexDirection: "row",
    height: 60,
    margin: 28,
  },
  icon: {
    marginLeft: 12,
  },
  input: {
    flex: 1,
    fontSize: 17,
    marginHorizontal: 7,
  },
});

export default React.memo(SearchBar);
