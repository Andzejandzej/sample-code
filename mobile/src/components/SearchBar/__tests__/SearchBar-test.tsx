import React from "react";
import { render, fireEvent } from "react-native-testing-library";
import SearchBar from "../SearchBar";

const mockedSetSearchPhrase = jest.fn();

describe("<SearchBar />", () => {
  it("Should call setSearchPhrase with adequate argument on typing into the input", () => {
    const { getByPlaceholder } = render(
      <SearchBar setSearchPhrase={mockedSetSearchPhrase} />
    );
    const instance = getByPlaceholder("Search...");
    fireEvent.changeText(instance, "some phrase");
    expect(mockedSetSearchPhrase).toHaveBeenCalledWith("some phrase");
  });
});
