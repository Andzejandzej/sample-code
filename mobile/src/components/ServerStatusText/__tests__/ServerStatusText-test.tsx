import React from "react";
import { render } from "react-native-testing-library";
import { ServerStatuses } from "../../../constants";
import ServerStatusText from "../ServerStatusText";

describe("<ServerStatusText />", () => {
  it("Renders 'ONLINE' text when passed status='ONLINE' prop", () => {
    const { getAllByText } = render(
      <ServerStatusText status={ServerStatuses.ONLINE} />
    );
    expect(getAllByText("ONLINE")).toHaveLength(1);
  });

  it("Renders 'OFFLINE' text when passed status='OFFLINE' prop", () => {
    const { getAllByText } = render(
      <ServerStatusText status={ServerStatuses.OFFLINE} />
    );
    expect(getAllByText("OFFLINE")).toHaveLength(1);
  });

  it("Renders 'REBOOTING...' text when passed status='REBOOTING' prop", () => {
    const { getAllByText } = render(
      <ServerStatusText status={ServerStatuses.REBOOTING} />
    );
    expect(getAllByText("REBOOTING...")).toHaveLength(1);
  });
});
