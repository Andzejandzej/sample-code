import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/Entypo";
import { ServerStatuses } from "../../constants";
import { colors } from "../../styles";

type ServerStatusTextProps = {
  status: "ONLINE" | "OFFLINE" | "REBOOTING";
};

const ServerStatusText: React.FC<ServerStatusTextProps> = ({ status }) => {
  switch (status) {
    case ServerStatuses.ONLINE:
      return (
        <View style={styles.container}>
          <View style={styles.dot} />
          <Text style={styles.onlineText}>ONLINE</Text>
        </View>
      );
    case ServerStatuses.OFFLINE:
      return (
        <View style={styles.container}>
          <View style={styles.crossIconContainer}>
            <Icon name="cross" color={colors.pinkRed} size={20} />
          </View>
          <Text style={styles.notOnlineText}>OFFLINE</Text>
        </View>
      );
    case ServerStatuses.REBOOTING:
      return (
        <View style={styles.container}>
          <Text style={styles.notOnlineText}>REBOOTING...</Text>
        </View>
      );
    default:
      throw `Unknown status of ${status}`;
  }
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flexDirection: "row",
  },
  crossIconContainer: {
    marginLeft: -5,
  },
  dot: {
    backgroundColor: colors.aqua,
    borderRadius: 4,
    height: 8,
    marginRight: 5,
    width: 8,
  },
  notOnlineText: {
    color: colors.violetGray,
  },
  onlineText: {
    color: colors.aqua,
  },
});

export default React.memo(ServerStatusText);
