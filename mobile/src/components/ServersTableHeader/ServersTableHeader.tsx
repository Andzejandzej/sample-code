import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { colors } from "../../styles";

const ServersTableHeader: React.FC = () => {
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Text style={styles.name}>NAME</Text>
        <Text style={styles.status}>STATUS</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    borderTopLeftRadius: 5,
  },
  content: {
    alignItems: "center",
    borderBottomColor: colors.darkBlue,
    borderBottomWidth: 1,
    flexDirection: "row",
    height: 62,
    marginLeft: 28,
  },
  name: {
    color: colors.lightBlue,
    flex: 215,
    fontWeight: "bold",
  },
  status: {
    color: colors.lightBlue,
    flex: 155,
    fontWeight: "bold",
  },
});

export default ServersTableHeader;
