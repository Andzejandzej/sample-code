import axios, { AxiosPromise } from "axios";
import { ServersType, ServerType } from "./servers-types";

const API_URL = "http://10.0.2.2:4454";
const axiosInstance = axios.create({
  baseURL: API_URL,
});

const getServers = (): AxiosPromise<ServersType> => {
  return axiosInstance.get("/servers");
};

type setServerStatusType = (
  serverId: number,
  targetStatus: "on" | "off"
) => AxiosPromise<ServerType>;

const setServerStatus: setServerStatusType = (serverId, targetStatus) => {
  return axiosInstance.put(`/servers/${serverId}/${targetStatus}`);
};

type rebootServerType = (serverId: number) => AxiosPromise;

const rebootServer: rebootServerType = (serverId) => {
  return axiosInstance.put(`/servers/${serverId}/reboot`);
};

type getServerType = (serverId: number) => AxiosPromise;

const getServer: getServerType = (serverId) => {
  return axiosInstance.get(`/servers/${serverId}`);
};

export default {
  getServer,
  getServers,
  rebootServer,
  setServerStatus,
};
