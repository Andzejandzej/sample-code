export type ServerType = {
  id: number;
  name: string;
  status: "ONLINE" | "OFFLINE" | "REBOOTING";
};
export type ServersType = Array<ServerType>;
