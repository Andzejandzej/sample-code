import React, { useContext, useState, useMemo } from "react";
import { View, StyleSheet } from "react-native";
import { ServersTable, ServerCountSection, SearchBar } from "../../components";
import { ServersContext } from "../../context";
import { colors } from "../../styles";

const ServersScreen: React.FC = () => {
  const { servers } = useContext(ServersContext);
  const [searchPhrase, setSearchPhrase] = useState<string>("");

  const filteredServers = useMemo(
    () =>
      servers.filter((server) =>
        server.name.toLowerCase().includes(searchPhrase.toLocaleLowerCase())
      ),
    [servers, searchPhrase]
  );

  return (
    <View style={styles.container}>
      <SearchBar setSearchPhrase={setSearchPhrase} />
      <ServerCountSection serverCount={filteredServers.length} />
      <ServersTable servers={filteredServers} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.whiteBlue,
    flex: 1,
  },
});

export default ServersScreen;
