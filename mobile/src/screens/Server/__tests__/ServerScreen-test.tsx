import { useRoute } from "@react-navigation/native";
import React from "react";
import { create } from "react-test-renderer";
import ServerScreen from "../ServerScreen";

jest.mock("@react-navigation/native", () => ({
  useNavigation: jest.fn(),
  useRoute: jest.fn() as jest.Mock,
}));
const mockedUseRoute = useRoute as jest.Mock;

describe("<ServerScreen />", () => {
  it("Shows only 'Turn on' checkbox", () => {
    mockedUseRoute.mockReturnValueOnce({
      params: { server: { id: 4, name: "some name", status: "OFFLINE" } },
    });
    const root = create(<ServerScreen />).root;
    expect(
      root.findByProps({ "data-testID": "turn on checkbox" })
    ).toBeDefined();
    expect(() =>
      root.findByProps({ "data-testID": "turn off checkbox" })
    ).toThrow();
    expect(() =>
      root.findByProps({ "data-testID": "reboot checkbox" })
    ).toThrow();
  });

  it("Shows only 'Turn off' and 'Reboot' checkboxes", () => {
    mockedUseRoute.mockReturnValueOnce({
      params: { server: { id: 4, name: "some name", status: "ONLINE" } },
    });
    const root = create(<ServerScreen />).root;
    expect(() =>
      root.findByProps({ "data-testID": "turn on checkbox" })
    ).toThrow();
    expect(
      root.findByProps({ "data-testID": "turn off checkbox" })
    ).toBeDefined();
    expect(
      root.findByProps({ "data-testID": "reboot checkbox" })
    ).toBeDefined();
  });
});
