import { useNavigation, useRoute, RouteProp } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import React, { useState, useContext, useCallback } from "react";
import { View, StyleSheet } from "react-native";
import {
  ServerStatusCheckbox,
  AcceptServerStatusChangeBtn,
} from "../../components";
import { ServerStatuses } from "../../constants";
import { ServersContext } from "../../context";
import { ServersStackParamList } from "../../navigation";

const ServerScreen: React.FC = () => {
  const { rebootServer, setServerStatus } = useContext(ServersContext);
  const navigation = useNavigation<
    StackNavigationProp<ServersStackParamList, "Server">
  >();
  const route = useRoute<RouteProp<ServersStackParamList, "Server">>();
  const server = route.params.server;
  const [selectedState, setSelectedState] = useState(
    server.status === ServerStatuses.ONLINE
      ? ServerStatuses.OFFLINE
      : ServerStatuses.ONLINE
  );

  const submit = useCallback((): void => {
    switch (selectedState) {
      case ServerStatuses.ONLINE:
        setServerStatus(server.id, "on");
        break;
      case ServerStatuses.OFFLINE:
        setServerStatus(server.id, "off");
        break;
      case ServerStatuses.REBOOTING:
        rebootServer(server.id);
        break;
    }
    navigation.goBack();
  }, [server.id, navigation, rebootServer, setServerStatus, selectedState]);

  return (
    <View style={styles.screen}>
      {server.status === ServerStatuses.OFFLINE && (
        <ServerStatusCheckbox
          data-testID="turn on checkbox"
          label="Turn on"
          isChecked={true}
          onCheckHandler={() => setSelectedState(ServerStatuses.ONLINE)}
        />
      )}
      {server.status === ServerStatuses.ONLINE && (
        <>
          <ServerStatusCheckbox
            data-testID="turn off checkbox"
            label="Turn off"
            isChecked={selectedState === ServerStatuses.OFFLINE}
            onCheckHandler={() => setSelectedState(ServerStatuses.OFFLINE)}
          />
          <ServerStatusCheckbox
            data-testID="reboot checkbox"
            label="Reboot"
            isChecked={selectedState === ServerStatuses.REBOOTING}
            onCheckHandler={() => setSelectedState(ServerStatuses.REBOOTING)}
          />
        </>
      )}
      <AcceptServerStatusChangeBtn onPressHandler={submit} />
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    alignItems: "center",
    flex: 1,
  },
});

export default ServerScreen;
