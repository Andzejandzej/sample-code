const servers = [
  {
    id: 1,
    name: " US East (Virginia)",
    status: "ONLINE",
  },
  {
    id: 2,
    name: " US East (Ohio)",
    status: "ONLINE",
  },
  {
    id: 3,
    name: "US West (N. California)",
    status: "OFFLINE",
  },
  {
    id: 4,
    name: " US West (Oregon)",
    status: "ONLINE",
  },
  {
    id: 5,
    name: " Asia Pacific (Mumbai)",
    status: "ONLINE",
  },
];

export default {
  create: jest.fn(() => ({
    get: jest.fn(() => Promise.resolve({ data: servers })),
  })),
};
