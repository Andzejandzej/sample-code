module.exports = {
  root: true,
  extends: ["react-native-typescript"],
  ignorePatterns: [
    "babel.config.js",
    "jest.config.js",
    "jest.setup.js",
    "metro.config.js",
  ],
};
